#include <stdio.h>
struct student {
    char name[20];
    char subject[20];
    int snum;
    float marks;
} s[10];

int main() {
    int i;
    printf("Enter information of students:\n");

    for (i = 0; i < 5; ++i) {
        s[i].snum = i + 1;
        printf("\nFor Student %d,\n", s[i].snum);
        printf("Enter First Name: ");
        scanf("%s", s[i].name);
        printf("Enter Subject: ");
        scanf("%s", s[i].subject);
        printf("Enter Marks: ");
        scanf("%f", &s[i].marks);
}

    printf("\nStudent Information:\n");

    for (i = 0; i < 5; ++i) {
        printf("\nStudent number: %d\n", i + 1);
        printf("First Name: ");
        puts(s[i].name);
        printf("Subject: ");
        puts(s[i].subject);
        printf("Marks: %.2f", s[i].marks);
        printf("\n--------------------------------\n");
}

  return 0;

}
